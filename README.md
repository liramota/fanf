# The Savings of Corporate Giants #

This repository contains data and code reference for the [The Savings of Corporate Giants (2023)](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3543802), by Olivier 
Darmouni and Lira Mota. 

### What is this repository for? ###

Here you can find resouces to study the financal assets of the 200 largest nonfinancial firms in the United States. 
The data was hand collected from the 10-K filings from 2000 until 2021. 

The financial assets is collected directly from the financial instruments footnote. The data granularity is based on the
data availability. 


### Data Download ####

* You can download the financial assets of nonfinancial firms [here](https://www.dropbox.com/sh/hnml5bx1kithk9y/AABWsUdA1b9EDSJYrtggH-dVa?dl=0).
	* "fis_annual_pub.xlsx" contains a breakdown of financial assets for each firm-year in our sample.  
	* "fis_quarterly_pub.xlsx" adds quarterly information for 2020 holdings, when available. 
	* "fis_agg_data_pub.xlsx"  aggregates the firm-level data to the year level.
	* "raw_data" folder contains a spreadsheet for each firm in our sample, using CIK as identifiers
		* "fis_table" subfolder contains the standartized data;
		* "financial_instruments" contains original tables from annual reports;
		* "fis_notes" contains data collection notes.
* Details on each field definition can be found [here](https://docs.google.com/spreadsheets/d/1Fm9r2KXCZu_hkv2um0bCix-vIFapUTu0tI6GU8BUu7Y/edit?usp=sharing).

### Replication Code

*Coming soon.*

### Guidelines ###

If you decide to use the resouces from this repository, please cite: 


```
Darmouni, Olivier and Mota, Lira, The Savings of Corporate Giants, 
Working Paper 2023. 
```

### Feedback ###
All errors are our own - but we would be very grateful to receive your help to fix them. 
Please email us if you find errors or more efficient solutions.

* Olivier Darmouni, o.darmouni@gmail.com
* Lira Mota, liramota@mit.edu

